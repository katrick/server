package com.chatlog.server.exceptions;

import com.chatlog.server.domain.ErrorCode;

public class InvalidUserException extends RuntimeException{

    String message;

    ErrorCode errorCode;

    public InvalidUserException(ErrorCode errorCode,String message) {
        super(message);
        this.message = message;
        this.errorCode = errorCode;
    }
}
