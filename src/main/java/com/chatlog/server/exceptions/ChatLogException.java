package com.chatlog.server.exceptions;

import com.chatlog.server.domain.ErrorCode;

public class ChatLogException extends RuntimeException{

    String message;

    ErrorCode errorCode;

    public ChatLogException(ErrorCode errorCode,String message) {
        super(message);
        this.message = message;
        this.errorCode = errorCode;
    }


}
