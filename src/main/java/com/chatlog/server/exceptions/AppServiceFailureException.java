package com.chatlog.server.exceptions;

import com.chatlog.server.domain.ErrorCode;

import javax.xml.catalog.CatalogException;

public class AppServiceFailureException extends CatalogException {
    String message;

    ErrorCode errorCode;

    public AppServiceFailureException(ErrorCode errorCode, String message) {
        super(message);
        this.message = message;
        this.errorCode = errorCode;
    }
}
