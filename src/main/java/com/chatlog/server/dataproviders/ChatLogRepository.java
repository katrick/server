package com.chatlog.server.dataproviders;

import com.chatlog.server.domain.ChatLog;
import com.chatlog.server.domain.ErrorCode;
import com.chatlog.server.exceptions.ChatLogException;
import com.chatlog.server.exceptions.InvalidUserException;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

@Repository
public class ChatLogRepository {

     private Map<String, List<ChatLog>> chatLogs;


    public ChatLogRepository() {
        this.chatLogs = new HashMap<>();
    }

    public String addChatLog(String username,ChatLog chatLog){

        if(chatLogs.get(username) == null)
            chatLogs.put(username,new ArrayList<>());

        List<ChatLog> logs = chatLogs.get(username);

         if(!logs.add(chatLog))
            throw new ChatLogException(ErrorCode.MESSAGE_NOT_SENT,"Failed to deliver the message");

        Collections.sort(logs, (o1, o2) -> (int) (o1.getTimeStamp()-o2.getTimeStamp()));

        chatLogs.put(username,logs);
;
       return chatLog.getId();
    }

    public List<ChatLog> getChatLog(String username) {

        if(chatLogs.get(username) == null)
            throw new InvalidUserException(ErrorCode.INVALID_USER,"User Not Found");

        return (chatLogs.get(username) != null)?chatLogs.get(username): Collections.emptyList();
    }

    public int deleteChatLog(String username) {

        if(chatLogs.get(username) == null)
            throw new InvalidUserException(ErrorCode.INVALID_USER,"User Not Found");

        int count = chatLogs.get(username).size();
        chatLogs.put(username,new ArrayList<>());

        return count;
    }

    public int deleteChatLogById(String username,String id) {

        if(chatLogs.get(username) == null)
            throw new InvalidUserException(ErrorCode.INVALID_USER,"User Not Found");

        List<ChatLog> logs = chatLogs.get(username).stream().filter(chat->chat.getId().equals(id)).collect(Collectors.toList());

        if(logs.isEmpty())
            throw new ChatLogException(ErrorCode.DATA_NOT_FOUND,"Invalid Chat Id");

        chatLogs.put(username,chatLogs.get(username).stream().filter(chatLog -> !chatLog.getId().equals(id)).collect(Collectors.toList()));

        return 1;
    }

}
