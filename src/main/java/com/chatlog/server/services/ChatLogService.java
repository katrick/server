package com.chatlog.server.services;

import com.chatlog.server.dataproviders.ChatLogRepository;
import com.chatlog.server.domain.ChatLog;
import com.chatlog.server.domain.ChatLogFactory;
import com.chatlog.server.domain.ErrorCode;
import com.chatlog.server.entrypoints.dto.ChatLogDto;
import com.chatlog.server.entrypoints.response.ChatLogCreatedResponse;
import com.chatlog.server.entrypoints.response.ChatLogsResponse;
import com.chatlog.server.entrypoints.response.DeletedChatLogsResponse;
import com.chatlog.server.entrypoints.validators.ChatLogInputValidator;
import com.chatlog.server.exceptions.AppServiceFailureException;
import com.chatlog.server.exceptions.ChatLogException;
import com.chatlog.server.exceptions.InvalidUserException;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ChatLogService {

    private ChatLogRepository logRepository;
    private ChatLogInputValidator chatLogInputValidator;

    public ChatLogService(ChatLogRepository logRepository,ChatLogInputValidator chatLogInputValidator) {
        this.logRepository = logRepository;
        this.chatLogInputValidator = chatLogInputValidator;
    }

    public ChatLogCreatedResponse addUserChat(String username, ChatLogDto chatLogDto) {
        ChatLog chatLog = new ChatLogFactory().createChatLog(chatLogDto);
        try {
            logRepository.addChatLog(username, chatLog);
        } catch (ChatLogException e) {
            throw new AppServiceFailureException(ErrorCode.INVALID_REQUEST,e.getMessage());
        }
        return new ChatLogCreatedResponse(username,chatLog.getId());
    }

    public ChatLogsResponse getUserChatHistory(String username, String startId, String limit) {
        try {
            List<ChatLog> chatLogs  = filterChatLogs(logRepository.getChatLog(username),startId,limit);
            ChatLogsResponse chatLogsResponse = new ChatLogsResponse(username,chatLogs);
            return chatLogsResponse;
        } catch (InvalidUserException e) {
            throw new AppServiceFailureException(ErrorCode.INVALID_REQUEST,e.getMessage());
        }
    }

    private List<ChatLog> filterChatLogs(List<ChatLog> chatLog, String startId, String limit) {

        startId = (startId != null)?startId:"";

        limit = (limit != null)?limit:"10";

        if(!chatLog.isEmpty()){

            if(!startId.isEmpty()){
                String finalStartId = startId;
                List<ChatLog> chatToBegin = chatLog.stream().filter(chat -> chat.getId().equals(finalStartId)).collect(Collectors.toList());
                if(chatToBegin.isEmpty())
                    throw new ChatLogException(ErrorCode.MESSAGE_NOT_FOUND,"Message startId is invalid.");

                List<ChatLog> result = chatLog.subList(0,(chatLog.size()-1>chatLog.indexOf(chatToBegin.get(0))+1)?chatLog.indexOf(chatToBegin.get(0))+1:chatLog.size());

                Collections.reverseOrder();

                result = result.subList(0,(result.size()<Integer.valueOf(limit))?result.size():Integer.valueOf(limit));

                sortResult(result);

               return result;

            }else{
                List<ChatLog> result = (chatLog.size()<Integer.valueOf(limit))?chatLog:chatLog.subList(chatLog.size() - Integer.valueOf(limit) -1,chatLog.size());
                sortResult(result);
                return result;
            }
        }

        return chatLog;
    }

    private void sortResult(List<ChatLog> result) {

        Collections.sort(result, (o1, o2) -> (int) (o2.getTimeStamp() - o1.getTimeStamp()));
    }

    public DeletedChatLogsResponse deleteUserChats(String username) {
        try {
            int chatCount = logRepository.deleteChatLog(username);
            return new DeletedChatLogsResponse(username,chatCount);
        } catch (InvalidUserException e) {
            throw new AppServiceFailureException(ErrorCode.INVALID_REQUEST,e.getMessage());
        }
    }

    public DeletedChatLogsResponse deleteUserChatId(String username,String id){
        try {
            chatLogInputValidator.validate(id);
            return new DeletedChatLogsResponse(username,logRepository.deleteChatLogById(username,id));
        } catch (ChatLogException e) {
            throw new AppServiceFailureException(ErrorCode.INVALID_REQUEST,e.getMessage());
        }
    }
}
