package com.chatlog.server.domain;

public enum ErrorCode {

    INVALID_REQUEST, DATA_NOT_FOUND, MESSAGE_NOT_SENT, INVALID_USER, MESSAGE_NOT_FOUND, INVALID_MESSAGE_ID
}
