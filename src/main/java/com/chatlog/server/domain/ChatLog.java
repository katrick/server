package com.chatlog.server.domain;

import com.chatlog.server.util.IdGenerator;

public class ChatLog {

    private String id;
    private String message;

    private long timeStamp;

    private boolean isSent;

    public ChatLog(String message, long timeStamp, boolean isSent) {
        this.id = IdGenerator.generateId();
        this.message = message;
        this.timeStamp = timeStamp;
        this.isSent = isSent;
    }

    public String getMessage() {
        return message;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public boolean isSent() {
        return isSent;
    }

    public String getId() {
        return id;
    }


}
