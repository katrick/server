package com.chatlog.server.domain;

import com.chatlog.server.entrypoints.dto.ChatLogDto;

public class ChatLogFactory {

    public ChatLog createChatLog(ChatLogDto chatLogDto){
        return new ChatLog(chatLogDto.getMessage(),chatLogDto.getTimestamp(),chatLogDto.sent());
    }
}
