package com.chatlog.server.entrypoints.validators;

import com.chatlog.server.domain.ErrorCode;
import com.chatlog.server.entrypoints.dto.ChatLogDto;
import com.chatlog.server.exceptions.ChatLogException;
import org.springframework.stereotype.Component;

@Component
public class ChatLogInputValidator {

    public boolean validate(String... args){
        for(String s : args){
            if(s== null || s.trim().length()==0)
                throw new ChatLogException(ErrorCode.INVALID_MESSAGE_ID,"Invalid MessageId.");
        }
         return false;
    }

    public void validateUserName(String userName){
        if(userName.length()>16)
            throw new ChatLogException(ErrorCode.INVALID_REQUEST,"Invalid User Name.");
    }

    public boolean validateDto(ChatLogDto chatLogDto){

        if(chatLogDto.getMessage() == null || chatLogDto.getMessage().trim().length() == 0)
            throw new ChatLogException(ErrorCode.INVALID_REQUEST,"Invalid Message Type");

        if(chatLogDto.getTimestamp() <= 0)
            throw new ChatLogException(ErrorCode.INVALID_REQUEST,"Invalid Message Time");

        return false;
    }
}
