package com.chatlog.server.entrypoints.dto;

public class ChatLogDto {
    private String message;

    private long timestamp;

    private boolean sent;

    public String getMessage() {
        return message;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public boolean sent() {
        return sent;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

}
