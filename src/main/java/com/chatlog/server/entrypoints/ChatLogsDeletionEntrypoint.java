package com.chatlog.server.entrypoints;

import com.chatlog.server.entrypoints.dto.ChatLogDto;
import com.chatlog.server.entrypoints.response.ChatLogCreatedResponse;
import com.chatlog.server.entrypoints.response.DeletedChatLogsResponse;
import com.chatlog.server.entrypoints.response.FailureResponseComposer;
import com.chatlog.server.entrypoints.validators.ChatLogInputValidator;
import com.chatlog.server.services.ChatLogService;
import com.google.gson.Gson;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Entrypoint responsible for chat log deletion.
 */
@RestController
public class ChatLogsDeletionEntrypoint {

    private ChatLogService chatLogService;

    private ChatLogInputValidator chatLogInputValidator;

    public ChatLogsDeletionEntrypoint(ChatLogService chatLogService, ChatLogInputValidator chatLogInputValidator) {
        this.chatLogService = chatLogService;
        this.chatLogInputValidator = chatLogInputValidator;
    }

    @DeleteMapping(value = "/chatlogs/{user}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> deleteChatLog(@PathVariable("user") String username) {
        try {
            chatLogInputValidator.validateUserName(username);
            DeletedChatLogsResponse deletedChatLogsResponse = chatLogService.deleteUserChats(username);
            return ResponseEntity.status(HttpStatus.OK).body(new Gson().toJson(deletedChatLogsResponse));
        }
        catch (Exception ex) {
            return FailureResponseComposer.composeFailureResponse(ex, username);
        }
    }

    @DeleteMapping(value = "/chatlogs/{user}/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> deleteChatLogById(@PathVariable("user") String username,@PathVariable("id") String messageId) {
        try {
            chatLogInputValidator.validateUserName(username);
            DeletedChatLogsResponse deletedChatLogsResponse = chatLogService.deleteUserChatId(username,messageId);
            return ResponseEntity.status(HttpStatus.OK).body(new Gson().toJson(deletedChatLogsResponse));
        }
        catch (Exception ex) {
            return FailureResponseComposer.composeFailureResponse(ex, username);
        }
    }

}
