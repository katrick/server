package com.chatlog.server.entrypoints;

import com.chatlog.server.entrypoints.response.ChatLogsResponse;
import com.chatlog.server.entrypoints.response.FailureResponseComposer;
import com.chatlog.server.entrypoints.validators.ChatLogInputValidator;
import com.chatlog.server.services.ChatLogService;
import com.google.gson.Gson;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;

/**
 * Entrypoint responsible for chat log retrieval.
 */
@RestController
public class ChatLogsRetrievalEntrypoint {
    private ChatLogService chatLogService;
    private ChatLogInputValidator chatLogInputValidator;

    public ChatLogsRetrievalEntrypoint(ChatLogService chatLogService, ChatLogInputValidator chatLogInputValidator) {
        this.chatLogService = chatLogService;
        this.chatLogInputValidator = chatLogInputValidator;
    }

    @GetMapping(value = "/chatlogs/{user}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> chatLogs(@PathVariable("user") String username, @Nullable @RequestParam("start") String startId,@Nullable @RequestParam("limit") String limit) {

        try {
            chatLogInputValidator.validateUserName(username);
            ChatLogsResponse chatLogsResponse = chatLogService.getUserChatHistory(username,startId,limit);

            return ResponseEntity.status(HttpStatus.OK).body(new Gson().toJson(chatLogsResponse));
        }
        catch (Exception ex) {
            return FailureResponseComposer.composeFailureResponse(ex, username);
        }
    }

}
