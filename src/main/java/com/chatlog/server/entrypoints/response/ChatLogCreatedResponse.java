package com.chatlog.server.entrypoints.response;

public class ChatLogCreatedResponse {

    private String userName;

    private String messageId;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public ChatLogCreatedResponse(String userName, String messageId) {
        this.userName = userName;
        this.messageId = messageId;
    }
}
