package com.chatlog.server.entrypoints.response;

import com.chatlog.server.exceptions.AppServiceFailureException;
import com.chatlog.server.exceptions.ChatLogException;
import com.chatlog.server.exceptions.InvalidUserException;
import com.google.gson.Gson;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;

public class FailureResponseComposer {

    private FailureResponseComposer() {
    }

    private static ResponseEntity.BodyBuilder handleFailure(Exception ex) {

        if (ex instanceof ChatLogException) {
            return compose400BadRequestResponse();
        }
        else if (ex instanceof InvalidUserException) {
            return compose404RequestNotFoundResponse();
        }else if (ex instanceof AppServiceFailureException) {
            return compose400BadRequestResponse();
        }
        else {
            return compose500FailureResponse();
        }
    }

    private static ResponseEntity.BodyBuilder compose400BadRequestResponse() {
        return ResponseEntity.badRequest();
    }

    private static ResponseEntity.BodyBuilder compose500FailureResponse() {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private static ResponseEntity.BodyBuilder compose404RequestNotFoundResponse() {
        return (ResponseEntity.BodyBuilder) ResponseEntity.notFound();
    }

    public static ResponseEntity<String> composeFailureResponse(Exception ex,String user) {

        ResponseEntity.BodyBuilder responseEntity = handleFailure(ex);

        ResponseBody response = new ResponseBody(user, Collections.emptyMap());

        response.setError(ex.getMessage());

        return responseEntity.body(new Gson().toJson(response));
    }

}

