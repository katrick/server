package com.chatlog.server.entrypoints.response;

public class DeletedChatLogsResponse {

    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getDeletedChatCount() {
        return deletedChatCount;
    }

    public void setDeletedChatCount(int deletedChatCount) {
        this.deletedChatCount = deletedChatCount;
    }

    private int deletedChatCount;

    public DeletedChatLogsResponse(String username, int deletedChatCount) {
        this.username = username;
        this.deletedChatCount = deletedChatCount;
    }


}
