package com.chatlog.server.entrypoints.response;

import java.util.Map;

public class ResponseBody {

    private String userName;

    private Map<String,Object> data;

    private String error;

    public ResponseBody(String userName, Map<String, Object> data) {
        this.userName = userName;
        this.data = data;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "{" +
                "userName='" + userName + '\'' +
                ", data=" + data +
                ", error='" + error + '\'' +
                '}';
    }
}
