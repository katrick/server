package com.chatlog.server.entrypoints.response;

import com.chatlog.server.domain.ChatLog;
import java.util.*;

public class ChatLogsResponse {

    private String userName;
    private List<ChatLog> chatLogs;

    private int noOfPages;

    private int totalRecord;



    public ChatLogsResponse(String userName, List<ChatLog> chatLogs) {
        this.userName = userName;
        this.chatLogs = chatLogs;
        this.totalRecord = chatLogs.size();
        this.noOfPages = chatLogs.size()/10;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<ChatLog> getChatLogs() {
        return chatLogs;
    }

    public void setChatLogs(List<ChatLog> chatLogs) {
        this.chatLogs = chatLogs;
    }

}
