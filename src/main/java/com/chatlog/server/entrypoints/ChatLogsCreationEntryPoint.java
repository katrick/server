package com.chatlog.server.entrypoints;

import com.chatlog.server.entrypoints.dto.ChatLogDto;
import com.chatlog.server.entrypoints.response.ChatLogCreatedResponse;
import com.chatlog.server.entrypoints.response.FailureResponseComposer;
import com.chatlog.server.entrypoints.validators.ChatLogInputValidator;
import com.chatlog.server.services.ChatLogService;
import com.google.gson.Gson;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Entrypoint responsible for chat log creation.
 */
@RestController
public class ChatLogsCreationEntryPoint {

    private ChatLogInputValidator chatLogInputValidator;

    private ChatLogService chatLogService;

    public ChatLogsCreationEntryPoint(ChatLogInputValidator chatLogInputValidator, ChatLogService chatLogService) {
        this.chatLogInputValidator = chatLogInputValidator;
        this.chatLogService = chatLogService;
    }


    @PostMapping(value = "/chatlogs/{user}", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> createChatLog(@PathVariable("user") String username,
                                          @RequestBody ChatLogDto chatLogDto) {
        try {
            chatLogInputValidator.validateDto(chatLogDto);chatLogInputValidator.validateUserName(username);

            ChatLogCreatedResponse chatLogCreatedResponse = chatLogService.addUserChat(username, chatLogDto);

            return ResponseEntity.status(HttpStatus.CREATED).body(new Gson().toJson(chatLogCreatedResponse));
        }
        catch (Exception ex) {
        return FailureResponseComposer.composeFailureResponse(ex, username);
    }
    }

}
